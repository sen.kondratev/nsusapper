import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import sun.font.TrueTypeFont;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class FieldTest {

    Cell cell = new Cell();
    Field f_mock = mock(Field.class);
    @Test
    public void isOpenedTest() {
        cell.changeOpenStatus();
        when(f_mock.isOpened(1,1)).thenReturn(cell.isOpened());
        assertTrue(f_mock.isOpened(1,1));
    }

    @Test
    public void isCheckedTest() {
        cell.changeCheckStatus();
        when(f_mock.isChecked(1,1)).thenReturn(cell.isChecked());
        assertTrue(f_mock.isChecked(1,1));
    }
    @Test
    public void isQuestionedTest() {
        cell.changeCheckStatus();
        cell.changeCheckStatus();
        when(f_mock.isQuestioned(1,1)).thenReturn(cell.isQuestioned());
        assertTrue(f_mock.isQuestioned(1,1));
    }
    @Test
    public void getCellContentTest() {
        when(f_mock.getCellContent(1,1)).thenReturn(cell.getContent());
        assertEquals(Cell.FREE, f_mock.getCellContent(1,1));
        cell.setContent(Cell.MINE);
        when(f_mock.getCellContent(1,1)).thenReturn(cell.getContent());
        assertEquals( Cell.MINE, f_mock.getCellContent(1,1));
    }
    @Test
    public void FieldTest() {
        Field f = new Field(10, 20, 10);
        Field f_spy = spy(f);
        when(f_spy.getXSize()).thenReturn(10);
        when(f_spy.getYSize()).thenReturn(20);
        when(f_spy.getMineCount()).thenReturn(10);
        assertEquals(f_spy.getXSize(), f.getXSize());
        assertEquals(f_spy.getYSize(),  f.getYSize());
        assertEquals(f_spy.getMineCount(), f.getMineCount());
    }
    @Test
    public void changeCheckStatusTest(){
        Field f_spy = spy(new Field(10, 10, 10));
        f_spy.changeCheckStatus(1,1);
        assertTrue(f_spy.isChecked(1,1) );
        f_spy.changeCheckStatus(1,1);
        assertTrue(f_spy.isQuestioned(1,1));
        f_spy.changeCheckStatus(1,1);
        assertFalse(f_spy.isQuestioned(1,1) || f_spy.isChecked(1,1));
    }
    @Test
    public void getOpenedCountTest(){
        when(f_mock.getOpenedCount()).thenReturn(10);
        assertEquals(10, f_mock.getOpenedCount());
    }
    @Test
    public void resetTest(){
        Field f_spy = spy(new Field(10, 10 ,20));
        f_spy.reset();
        assertEquals(0, f_spy.getOpenedCount());
        assertEquals(0, f_spy.getFlagCount());
    }
    @Test
    public void countUncheckedMinesTest(){
        Field f_spy = spy(new Field(10, 10, 10));
        assertEquals(f_spy.countUncheckedMines(), f_spy.getMineCount() - f_spy.getFlagCount());
    }

    @Test
    public void changeOpenStatusTest() throws GameException {
        Field f_spy = spy(new Field(10, 10, 10));
        f_spy.changeOpenStatus(1,1);
        assertTrue(f_spy.isOpened(1,1));
        f_spy.changeOpenStatus(1,1);
        assertTrue(f_spy.isOpened(1,1));

    }

}