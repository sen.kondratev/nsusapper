import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.Test;


import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class CellTest {

    @Test
    public void getContent() throws NoSuchFieldException, IllegalAccessException {
        Cell c = new Cell();
        Field field = c.getClass().getDeclaredField("content");
        field.setAccessible(true);
        field.set(c, true);
        Boolean result = c.getContent();
        assertEquals( true, result);
    }

    @Test
    public void setContent() throws NoSuchFieldException, IllegalAccessException {
        Cell c = new Cell();
        c.setContent(true);
        Field field = c.getClass().getDeclaredField("content");
        field.setAccessible(true);

        assertEquals( true, field.get(c));
    }

    @Test
    public void setInitialState() throws NoSuchFieldException, IllegalAccessException {
        Cell c = new Cell();
        c.setInitialState();
        Field fieldChecked = c.getClass().getDeclaredField("checked");
        Field fieldOpened = c.getClass().getDeclaredField("opened");
        Field fieldQuestioned = c.getClass().getDeclaredField("questioned");
        fieldChecked.setAccessible(true);
        fieldOpened.setAccessible(true);
        fieldQuestioned.setAccessible(true);

        assertEquals(false, fieldChecked.get(c));
        assertEquals(false, fieldOpened.get(c));
        assertEquals(false, fieldQuestioned.get(c));
        assertFalse(c.getContent());
    }

    @Test
    public void isOpened() {
        Cell c = new Cell();
        c.setInitialState();
        assertFalse(c.isOpened());
    }

    @Test
    public void isChecked() {
        Cell c = new Cell();
        c.setInitialState();
        assertFalse(c.isChecked());
    }

    @Test
    public void isQuestioned() {
        Cell c = new Cell();
        c.setInitialState();
        assertFalse(c.isQuestioned());
    }

    @Test
    public void changeOpenStatus() {
        Cell c = new Cell();
        c.setInitialState();
        c.changeOpenStatus();
        assertTrue(c.isOpened());
        c.changeOpenStatus();//не меняется ли на противоположное
        assertTrue(c.isOpened());
    }

    @Test
    public void open() throws GameException {
        Cell c = new Cell();
        c.setInitialState();
        c.open();
        assertTrue(c.isOpened());
        c.setContent(true);
        try{
            c.open();
        }
        catch (GameException e){
            assertEquals( 0, e.getType());
        }

    }


    @Test
    public void changeCheckStatus() {
       Cell c = new Cell();
       c.setInitialState();
       c.changeCheckStatus();
        assertTrue(c.isChecked());
        assertFalse(c.isQuestioned());

        c.changeCheckStatus();
        assertFalse(c.isChecked());
        assertTrue(c.isQuestioned());

        c.changeCheckStatus();
        assertFalse(c.isQuestioned());
        assertFalse(c.isChecked());
        //and help niago
    }



}