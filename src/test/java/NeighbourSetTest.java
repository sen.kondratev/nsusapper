import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.*;

public class NeighbourSetTest {

    @Test
    public void toArray() {
        NeighbourSet neighbourSet = new NeighbourSet(1,1, 10, 20);
        Dimension[] neighbourSetArray = neighbourSet.toArray();
        assertEquals(8, neighbourSetArray.length);
        NeighbourSet neighbourSet1 = new NeighbourSet(0,0,10,20);
        Dimension[] neighbourSetArray1 = neighbourSet1.toArray();
        assertEquals(3, neighbourSetArray1.length);
    }
}