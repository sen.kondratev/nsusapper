import javax.swing.*;
import java.awt.*;

public final class CustomMenu extends NewWindowPanel {
    private static final JTextField widthEditor = new JTextField();
    private static final JTextField heightEditor = new JTextField();
    private static final JTextField mineCountEditor = new JTextField();


    private static JLabel widthLabel = new JLabel("Width (between " + CreateFieldAction.MINIMUM_SIZE + " and "
            + CreateFieldAction.MAXIMUM_SIZE + "):");
    private static JLabel heightLabel = new JLabel("Height (between " + CreateFieldAction.MINIMUM_SIZE + " and "
            + CreateFieldAction.MAXIMUM_SIZE + "):");
    private static JLabel mineCountLabel = new JLabel("Mine count (less than cell count):");

    private static final Font FONT = new Font(Font.SERIF, Font.PLAIN, 15);

    private static final int GRID_HEIGHT = 4;
    private static final int GRID_WIDTH = 2;

    private static final Color USUAL_COLOR = Color.BLACK;
    private static final Color HIGHLITING_COLOR = Color.RED;

    public CustomMenu(GameFrame gameFrame) {
        super(new GridLayout(GRID_HEIGHT, GRID_WIDTH));

        JButton startGameButton = new JButton("Start Game");
        startGameButton.addActionListener(new CreateFieldAction(gameFrame));

        JButton returnButton = new JButton("Cancel");
        returnButton.addActionListener(new ReturnAction());

        widthLabel.setFont(FONT);
        heightLabel.setFont(FONT);
        mineCountLabel.setFont(FONT);

        add(widthLabel);
        add(widthEditor);
        add(heightLabel);
        add(heightEditor);
        add(mineCountLabel);
        add(mineCountEditor);
        add(startGameButton);
        add(returnButton);
    }
    public static int getFieldWidth()
    {
        return Integer.parseInt(widthEditor.getText());
    }

    public static int getFieldHeight()
    {
        return Integer.parseInt(heightEditor.getText());
    }

    public static int getMineCount()
    {
        return Integer.parseInt(mineCountEditor.getText());
    }
    public static void setHighlight(boolean widthHighlight, boolean heightHighlight, boolean mineCountHighlight)
    {
        widthLabel.setForeground(widthHighlight ? HIGHLITING_COLOR : USUAL_COLOR);
        heightLabel.setForeground(heightHighlight ? HIGHLITING_COLOR : USUAL_COLOR);
        mineCountLabel.setForeground(mineCountHighlight ? HIGHLITING_COLOR : USUAL_COLOR);
    }
}