import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class ExitAction implements ActionListener
{
    public void actionPerformed(ActionEvent event)
    {
        System.exit(1);
    }
}
