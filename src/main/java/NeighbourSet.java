import java.awt.*;
import java.util.HashSet;

final class NeighbourSet
{
    private static final int NEIGHBOUR_RADIUS = 1;
    private HashSet <Dimension> setOfNeighbours;

    public NeighbourSet(int xCell, int yCell, int xSize, int ySize)
    {
        setOfNeighbours = new HashSet <> ();
        for (int x = xCell - NEIGHBOUR_RADIUS; x <= xCell + NEIGHBOUR_RADIUS; x++)
        {
            if ((0 > x) || (x >= xSize))
            {
                continue;
            }
            for (int y = yCell - NEIGHBOUR_RADIUS; y <= yCell + NEIGHBOUR_RADIUS; y++)
            {
                if ((0 > y) || (y >= ySize) || ((xCell == x) && (yCell == y)))
                {
                    continue;
                }
                setOfNeighbours.add(new Dimension(x, y));
            }
        }
    }

    public Dimension[] toArray()
    {
        Dimension [] neighbourArray = new Dimension [setOfNeighbours.size()];
        setOfNeighbours.toArray(neighbourArray);
        return neighbourArray;
    }
}
