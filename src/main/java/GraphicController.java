import javax.swing.*;

public final class GraphicController
{
    private Field fieldModel;
    private GameTimer timer;
    private boolean gameEnded = false;
    private boolean gameOver = false;

    private int gameType;


    public GraphicController(Field fieldModel, int gameType)
    {
        this.fieldModel = fieldModel;
        this.gameType = gameType;
        timer = new GameTimer();
    }

    public void leftClick(int x, int  y) {
        try {
            fieldModel.changeOpenStatus(x, y);
            if ((0 < fieldModel.getOpenedCount()) && (!timer.isRunning()))
            {
                timer.start();
            }
        } catch (GameException exception) {
            timer.stop();
            gameEnded = true;
            if (GameException.GAME_OVER == exception.getType())
            {
                GamePanel.setState(GamePanel.LOOSE_STATE);
                 gameOver = true;
            }
            else
            {
                GamePanel.setState(GamePanel.WIN_STATE);

                if (GraphicMain.CUSTOM != gameType && ScoreProcessor.isTopScore(timer.getTime(), gameType))
                {
                    new GameDialog(new GetNameMenu(this));
                }
            }
        }
    }

    public void rightClick(int x, int y)
    {
        fieldModel.changeCheckStatus(x,y);
    }

    public boolean isGameOver()
    {
        return gameOver;
    }

    public boolean isGameEnded() {
        return gameEnded;
    }

    public void saveScore(String name)
    {
        ScoreProcessor.addScore(GameTimer.getTime(), name, gameType);
    }

    public void resetField()
    {
        gameEnded = false;
        gameOver = false;

        fieldModel.reset();
        timer.stop();
        timer.reset();
    }

    public int getUncheckedMineCount()
    {
        return fieldModel.countUncheckedMines();
    }

    public void setTimeLabel(JLabel timeLabel)
    {
        timer.setTimeLabel(timeLabel);
    }
}
