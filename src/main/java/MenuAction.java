import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class MenuAction implements ActionListener
{
    private JPanel destinationMenu;

    public MenuAction(JPanel destinationMenu)
    {
        this.destinationMenu = destinationMenu;
    }
    public void actionPerformed(ActionEvent event)
    {
        new GameDialog(destinationMenu);
    }

}