import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class RestartGameAction implements ActionListener
{
    private GraphicView fieldView;

    public RestartGameAction(GraphicView fieldView)
    {
        this.fieldView = fieldView;
    }

    public void actionPerformed(ActionEvent event)
    {
        fieldView.reset();
        GamePanel.setState(GamePanel.GAMING_STATE);
    }
}