import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class GetNameMenu extends NewWindowPanel
{
    private static final int BORDER_WIDTH = 3;
    private static final int NAME_FIELD_LENGTH = 10;

    public GetNameMenu(GraphicController controller)
    {
        super(new BorderLayout());

        JLabel textLabel = new JLabel("Congratulations! Your name:");
        textLabel.setBorder(BorderFactory.createLineBorder(Color.WHITE, BORDER_WIDTH));
        add(textLabel, BorderLayout.NORTH);

        JTextField playerNameField = new JTextField(NAME_FIELD_LENGTH);
        add(playerNameField, BorderLayout.CENTER);

        JButton readyButton = new JButton("Ok");
        readyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.saveScore(playerNameField.getText());
                new GameDialog(new ShowScoresMenu());
            }
        });
        readyButton.addActionListener(new ReturnAction());
        add(readyButton, BorderLayout.SOUTH);
    }
}
