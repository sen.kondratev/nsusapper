
import java.io.*;
import java.util.*;

public final class ScoreProcessor
{
    private static final String [] SCORE_FILE = {   "resource\\data\\noviceScores",
                                                    "resource\\data\\mediumScores",
                                                    "resource\\data\\expertScores"};
    private static final int SCORE_COUNT = 10;

    private ScoreProcessor() {

    }

    public static void addScore(int time, String userName, int gameType)
    {
        List <Score> scoreList = getScoreList(gameType);
        scoreList.add(new Score(userName, time));
        Collections.sort(scoreList);
        save (scoreList, gameType);
    }

    public static boolean isTopScore(int time, int gameType){
        if(getScoreList(gameType).size()>=SCORE_COUNT) {
            return time < getScoreList(gameType).get(SCORE_COUNT - 1).getTime();
        }else{
            return true;
        }
    }

    public static List <Score> getScoreList(int gameType)
    {
        List <Score> scoreList = new ArrayList<>();
        Reader reader = null;

        try
        {
            reader = new InputStreamReader(new FileInputStream(SCORE_FILE[gameType]));

            for (int scoreNumber = 0; reader.ready() && (scoreNumber < SCORE_COUNT); scoreNumber++)
            {
                StringBuilder name = new StringBuilder();
                StringBuilder time = new StringBuilder();

                while (reader.ready())
                {
                    char readingChar = (char)reader.read();

                    if (' ' == readingChar)
                    {
                        break;
                    }

                    name.append(readingChar);
                }

                while(reader.ready())
                {
                    char readingChar = (char)reader.read();

                    if (!Character.isDigit(readingChar))
                    {
                        break;
                    }

                    time.append(readingChar);
                }

                if (! ((name.toString().isEmpty()) || (time.toString().isEmpty())))
                {
                    Score newScore = new Score(name.toString(), Integer.parseInt(time.toString()));
                    scoreList.add(newScore);
                }
            }
        }
        catch (IOException catchValue)
        {
            System.err.println("Error while reading file: " + catchValue.getLocalizedMessage());
        }
        finally
        {
            if (null != reader)
            {
                try
                {
                    reader.close();
                }
                catch (IOException catchValue)
                {
                    catchValue.printStackTrace(System.err);
                }
            }
        }

        return scoreList;
    }

    private static void save(List<Score> scoreList, int gameType)
    {
        PrintWriter printWriter = null;

        try
        {
            printWriter = new PrintWriter(SCORE_FILE[gameType]);

            for (int index = 0; (index < SCORE_COUNT) && (index < scoreList.size()); index++)
            {
                printWriter.println(scoreList.get(index).toString());
            }
        }
        catch (FileNotFoundException catchValue)
        {
            System.err.println("Error while writing file: " + catchValue.getLocalizedMessage());
        }
        finally
        {
            if (null != printWriter)
            {
                printWriter.close();
            }
        }
    }
}
