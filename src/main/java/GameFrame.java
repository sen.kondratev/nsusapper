
import javax.swing.*;
import java.awt.*;


public final class GameFrame extends JFrame
{
    private static final int DEFAULT_LEVEL = GraphicMain.NOVICE;

    private static final Font MENU_FONT = new Font("Verdana", Font.PLAIN, 11);
    public GameFrame(String windowName)
    {
        super(windowName);

        JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();

        JMenu gameMenu = new JMenu("Game");
        JMenu newMenu = new JMenu("New");

        addStartGameItem("Novice", GraphicMain.NOVICE, newMenu);
        addStartGameItem("Medium", GraphicMain.MEDIUM, newMenu);
        addStartGameItem("Expert", GraphicMain.EXPERT, newMenu);

        JMenuItem customItem = new JMenuItem("Custom");
        customItem.addActionListener(new MenuAction(new CustomMenu(this)));
        customItem.setFont(MENU_FONT);
        newMenu.add(customItem);

        JMenuItem scoresItem = new JMenuItem("Scores");
        scoresItem.setFont(MENU_FONT);
        scoresItem.addActionListener(new MenuAction(new ShowScoresMenu()));
        
        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.setFont(MENU_FONT);

        gameMenu.add(newMenu);
        gameMenu.add(scoresItem);
        gameMenu.addSeparator();
        gameMenu.add(exitItem);

        exitItem.addActionListener(new ExitAction());
        menuBar.add(gameMenu);

        this.setJMenuBar(menuBar);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize);

        createStartField();

        setVisible(true);
    }


    private void addStartGameItem(String name, int gameType, JMenu parent)
    {
        JMenuItem newItem = new JMenuItem(name);
        newItem.addActionListener(new CreateFieldAction(this, gameType));
        newItem.setFont(MENU_FONT);
        parent.add(newItem);
    }
    private void createStartField()
    {
        int mode = DEFAULT_LEVEL;
        CreateFieldAction startField = new CreateFieldAction(this, mode);
        startField.actionPerformed(null);
    }

}

