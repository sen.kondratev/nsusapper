public final class Score implements Comparable<Score>{
    private String name;
    private int time;

    public Score(String initialName, int initialCount)  {
        name = initialName;
        time = initialCount;
    }

    public String toString() {
        return (name + " " + time);
    }

    public int getTime() {
        return time;
    }

    public String getName(){
        return name;
    }

    @Override
    public int compareTo(Score o) {
        return time - o.getTime();
    }
}